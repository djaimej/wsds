const bodyParser = require('body-parser');
const express = require("express");

require("dotenv").config();

const app = express();
const Sockets = require('./sockets');

const usersRoutes = require('./routes/users');
const authRoutes = require('./routes/auth');

const PORT = process.env.PORT || 3000;

app.use(express.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use("/users", usersRoutes);
app.use("/auth", authRoutes);

app.get("/", (req, res) => res.send("WSDS"));

const httpServer = require("http").createServer(app);
const options = {
    cors: {
        origin: "*",
        methods: ["GET", "POST"]
    }
};

const io = require("socket.io")(httpServer, options);
Sockets(io);

httpServer.listen(PORT);

console.log(`Server running on port: http://localhost:${PORT}`);