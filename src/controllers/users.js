const db = require('../db');

const getUsers = async(req, res) => {
    let response = null;
    await db.query('SELECT * FROM users').then(result => {
        response = result;
    }).catch(err => {
        console.log(err);
    }).finally(() =>
        res.send(response)
    );
};

const getUserById = async(req, res) => {
    console.log('datos: ', req.datosUsuario);
    const { id } = req.params;
    const { rows } = await db.query('SELECT * FROM users WHERE idUser = $1', [id]);
    res.send(rows[0]);
};

module.exports = {
    getUsers,
    getUserById
};