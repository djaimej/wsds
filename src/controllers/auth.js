const db = require('../db');
const jwt = require('jsonwebtoken');
const config = require('../config');

const Login = async(req, res) => {

    /* 
        req.body.password = 123335
        ivus67vrbttsa7b77cx

        ENCRYPT => req.body.password

        SI SON IGUALES ENTRA
    */

    await db.query('SELECT * FROM users WHERE username = $1 AND password = $2', [req.body.username, req.body.password]).then(result => {
        if (result.rowCount > 0) {
            const user = result.rows[0];
            const payload = {
                check: true,
                iduser: user.iduser,
                username: user.username,
                email: user.email
            };
            const token = jwt.sign(payload, config.jwtSecret, {
                expiresIn: 1440
            });
            res.json({
                mensaje: 'Autenticación correcta',
                token: token
            });
        } else {
            res.json({ mensaje: "Usuario o contraseña incorrectos" });
        }
    }).catch(err => {
        console.log(err);
    });
};

const Register = async(req, res) => {
    /* 
        TO DO
    */
};

module.exports = {
    Login,
    Register
};