module.exports = (io) => {
    io.on("connection", (socket) => {
        console.log(socket.handshake.url);
        console.log("new socket connected:", socket.id);

        // Send all messages to the client
        socket.emit("server:load", { msg: 'hello world' });

        socket.on("client:save", (msg) => {
            console.log(msg);
        });

        socket.on("disconnect", () => {
            console.log(socket.id, "disconnected");
        });
    });
};