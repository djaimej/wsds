const express = require('express');
const middleware = express.Router();
const jwt = require('jsonwebtoken');

const config = require('../config');

middleware.use((req, res, next) => {
    const token = req.headers.authorization.replace('Bearer ', '');
    if (token) {
        jwt.verify(token, config.jwtSecret, (err, decoded) => {
            if (err) {
                return res.json({ mensaje: 'invalid token' });
            } else {
                req.datosUsuario = decoded;
                next();
            }
        });
    } else {
        res.send({
            mensaje: 'Token no provided.'
        });
    }
});

module.exports = middleware;