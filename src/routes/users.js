const Router = require('express-promise-router');
const router = new Router();
const middleware = require('./middleware');

const { getUsers, getUserById } = require('../controllers/users.js');

router.get('/', middleware, getUsers);
router.get('/:id', middleware, getUserById);

module.exports = router;