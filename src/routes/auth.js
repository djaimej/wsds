const Router = require('express-promise-router');
const router = new Router();

const { Login, Register } = require('../controllers/auth.js');

router.post('/login', Login);
router.post('/register', Register);

module.exports = router;